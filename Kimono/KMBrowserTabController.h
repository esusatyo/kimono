//
//  KMBrowserTabController.h
//  Kimono
//
//  Created by James Dumay on 25/03/12.
//  Copyright (c) 2012 Atlassian. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>
#import "KMBrowserWindowController.h"
#import "KMBrowserErrorViewController.h"
#import "KMBrowserTab.h"
#import "KMPageUpdater.h"

@class KMBrowserWindowController;

@interface KMBrowserTabController : NSViewController <KMBrowserTab>

@property (strong) IBOutlet WebView *webView;
@property (strong) IBOutlet KMBrowserErrorViewController *errorViewController;
@property (retain, readonly) NSString *pageTitle;
@property (retain, readonly) NSImage *pageIcon;
@property (retain, readonly) NSURL *pageLocation;
@property (retain, readonly) id<KMPageUpdater> pageUpdater;

- (id)initWithPageUpdater:(id<KMPageUpdater>)pageUpdater;

- (void)loadURL:(NSURL*)url;
- (void)reload;
- (void)stopLoad;

- (void)showErrorView;
- (void)hideErrorView;

@end
