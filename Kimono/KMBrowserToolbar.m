//
//  KMBrowserToolbar.m
//  Kimono
//
//  Created by James Dumay on 28/03/12.
//  Copyright (c) 2012 Atlassian. All rights reserved.
//

#import "KMBrowserToolbar.h"

#import "KMBrowserWindowController.h"

@implementation KMBrowserToolbar

@synthesize timer = _timer;

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSShadow *dropShadow = [[NSShadow alloc] init];
        [dropShadow setShadowColor:[NSColor colorWithCalibratedRed:0.62 green:0.63 blue:0.62 alpha:1.00]];
        [dropShadow setShadowOffset:NSMakeSize(0, -1.0)];
        [dropShadow setShadowBlurRadius:1.0];
        
        [self setWantsLayer: YES];
        [self setShadow: dropShadow];
    }
    return self;
}

-(NSView*)toolbar
{
    KMBrowserWindowController *controller =  (KMBrowserWindowController*)[[self window] windowController];
    return [controller toolbar];
}

-(NSView *)innerView;
{
    KMBrowserWindowController *controller =  (KMBrowserWindowController*)[[self window] windowController];
    return [controller innerView];
}

-(NSView *)view
{
    KMBrowserWindowController *controller =  (KMBrowserWindowController*)[[self window] windowController];
    return [controller view];
}

-(NSTextField*)smartBar
{
    KMBrowserWindowController *controller =  (KMBrowserWindowController*)[[self window] windowController];
    return [controller smartBar];
}

-(NSPopover*)tabsPopover
{
    KMBrowserWindowController *controller =  (KMBrowserWindowController*)[[self window] windowController];
    return [controller tabsPopover];
}

-(void)showWithDelay:(NSString*)rectAsString  withView:(NSView*)view;
{
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
    [userInfo setObject:rectAsString forKey:@"rect"];
    [userInfo setObject:view forKey:@"view"];
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.8 target:self selector:@selector(showForTimer:) userInfo:userInfo repeats:NO];
}

-(void)showForTimer:(NSTimer*)timer
{
    NSDictionary *userInfo = [timer userInfo];
    NSRect rect = NSRectFromString([userInfo objectForKey:@"rect"]);
    NSView *view = [userInfo objectForKey:@"view"];
    NSRect toolbarFrame = NSMakeRect(rect.origin.x, rect.size.height - 36, rect.size.width, 36);

    [self setFrame:toolbarFrame];
    [[view animator] addSubview:self];
}

-(void)hideToolbar:(NSTimer*)timer
{
    [self hide];
}

-(void)hide
{
    [_timer invalidate];
    
    //    NSWindow *currentWindow = [[self view] window];
    if (![[self tabsPopover] isShown]) // && !([[self smartBar] isEqualTo:[currentWindow firstResponder]]))
    {
        [[[self toolbar] animator] removeFromSuperview];
    }
    else
    {
        _timer = [NSTimer scheduledTimerWithTimeInterval:0.8 target:self selector:@selector(hideToolbar:) userInfo:nil repeats:NO];
    }
}

- (void)drawRect:(NSRect)rect
{
    [[NSColor windowFrameColor] set];
    NSRectFill([self bounds]);
}

@end
