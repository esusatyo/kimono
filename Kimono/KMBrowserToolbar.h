//
//  KMBrowserToolbar.h
//  Kimono
//
//  Created by James Dumay on 28/03/12.
//  Copyright (c) 2012 Atlassian. All rights reserved.
//

#import <AppKit/AppKit.h>

@interface KMBrowserToolbar : NSView

@property (strong) NSTimer *timer;

-(NSView*)toolbar;

-(NSView *)innerView;

-(NSView *)view;

-(NSTextField*)smartBar;

-(NSPopover*)tabsPopover;

-(void)showWithDelay:(NSString*)rectAsString withView:(NSView*)view;

-(void)hide;

-(void)showForTimer:(NSTimer*)timer;

- (void)hideToolbar:(NSTimer *)timer;

@end
